package br.com.itau;

public class Vip extends Ingresso {

    protected double valorAdicional;

    public Vip(double valor, double valorAdicional) {
        super(valor);
        this.valorAdicional = valorAdicional;
    }

    @Override
    public double getValor(){
        return valor + valorAdicional;
    }

}
