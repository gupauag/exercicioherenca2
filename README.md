# exercicioHerenca2

Crie uma classe chamada Ingresso que possui um
valor em reais e um método imprimeValor().

a. crie uma classe VIP, que herda Ingresso e possui
um valor adicional. Crie um método que retorne o valor
do ingresso VIP (com o adicional incluído).

b. crie uma classe Normal, que herda Ingresso e
possui um método que imprime: "Ingresso Normal".

c. Crie uma classe Main para testar as classes e
métodos desenvolvidos.